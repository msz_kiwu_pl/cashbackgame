<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class FrontendController extends Controller {

    /**
     * @Route("/frontend", name="frontend")
     */
    public function indexAction() {



        return $this->render('default/frontend.html.twig', [
                    'a' => 'AAAAA',
                    'b' => 'BBBBBB',
        ]);
    }

    /**
     * @Route("/getdata", name="getdata")
     */
    public function getDataAction() {
        return new JsonResponse(array('name' => 'Masza-Jest-Nasza'));
    }

    /**
     * @Route("/getresp", name="getresp")
     */
    public function getRespAction() {

        $text = str_shuffle('POUGHKEEPSIE  ');
        return new Response($text);
    }

}
