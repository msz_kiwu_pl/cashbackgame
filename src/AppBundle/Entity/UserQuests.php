<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PostRepository")
 * @ORM\Table(name="cg_user_quests")
 */
class UserQuests {

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    private $userId;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    private $questId;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isRequest;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     * @Assert\DateTime
     */
    private $requestDate;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isDone;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     * @Assert\DateTime
     */
    private $doneDate;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return UserQuests
     */
    public function setUserId($userId) {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId() {
        return $this->userId;
    }

    /**
     * Set questId
     *
     * @param integer $questId
     *
     * @return UserQuests
     */
    public function setQuestId($questId) {
        $this->questId = $questId;

        return $this;
    }

    /**
     * Get questId
     *
     * @return integer
     */
    public function getQuestId() {
        return $this->questId;
    }

    /**
     * Set isRequest
     *
     * @param boolean $isRequest
     *
     * @return UserQuests
     */
    public function setIsRequest($isRequest) {
        $this->isRequest = $isRequest;

        return $this;
    }

    /**
     * Get isRequest
     *
     * @return boolean
     */
    public function getIsRequest() {
        return $this->isRequest;
    }

    /**
     * Set requestDate
     *
     * @param \DateTime $requestDate
     *
     * @return UserQuests
     */
    public function setRequestDate($requestDate) {
        $this->requestDate = $requestDate;

        return $this;
    }

    /**
     * Get requestDate
     *
     * @return \DateTime
     */
    public function getRequestDate() {
        return $this->requestDate;
    }

    /**
     * Set isDone
     *
     * @param boolean $isDone
     *
     * @return UserQuests
     */
    public function setIsDone($isDone) {
        $this->isDone = $isDone;

        return $this;
    }

    /**
     * Get isDone
     *
     * @return boolean
     */
    public function getIsDone() {
        return $this->isDone;
    }

    /**
     * Set doneDate
     *
     * @param \DateTime $doneDate
     *
     * @return UserQuests
     */
    public function setDoneDate($doneDate) {
        $this->doneDate = $doneDate;

        return $this;
    }

    /**
     * Get doneDate
     *
     * @return \DateTime
     */
    public function getDoneDate() {
        return $this->doneDate;
    }

}
