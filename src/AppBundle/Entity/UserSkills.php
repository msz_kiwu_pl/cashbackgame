<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="cg_user_skills")
 */
class UserSkills {

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    private $userId;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    private $skillId;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    private $points;

    function getId() {
        return $this->id;
    }

    function getUserId() {
        return $this->userId;
    }

    function getSkillId() {
        return $this->skillId;
    }

    function getPoints() {
        return $this->points;
    }

    function setId($id) {
        $this->id = $id;
        return $this;
    }

    function setUserId($userId) {
        $this->userId = $userId;
        return $this;
    }

    function setSkillId($skillId) {
        $this->skillId = $skillId;
        return $this;
    }

    function setPoints($points) {
        $this->points = $points;
        return $this;
    }

}
