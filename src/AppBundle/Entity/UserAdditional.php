<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;

/**
 * @ORM\Entity
 * @ORM\Table(name="cg_user_additional")
 */
class UserAdditional {

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $cashbackNo;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $lyconetNo;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $merchantNo;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $refLink;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $history;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $why;

    /**
     * @ORM\Column(type="string")
     *
     * @Assert\File(mimeTypes={ "image/jpeg" })
     */
    protected $photo;

    function getId() {
        return $this->id;
    }

    function getCashbackNo() {
        return $this->cashbackNo;
    }

    function getLyconetNo() {
        return $this->lyconetNo;
    }

    function getMerchantNo() {
        return $this->merchantNo;
    }

    function getRefLink() {
        return $this->refLink;
    }

    function getHistory() {
        return $this->history;
    }

    function getWhy() {
        return $this->why;
    }

    function getPhoto() {
        return $this->photo;
    }

    function setId($id) {
        $this->id = $id;
        return $this;
    }

    function setCashbackNo($cashbackNo) {
        $this->cashbackNo = $cashbackNo;
        return $this;
    }

    function setLyconetNo($lyconetNo) {
        $this->lyconetNo = $lyconetNo;
        return $this;
    }

    function setMerchantNo($merchantNo) {
        $this->merchantNo = $merchantNo;
        return $this;
    }

    function setRefLink($refLink) {
        $this->refLink = $refLink;
        return $this;
    }

    function setHistory($history) {
        $this->history = $history;
        return $this;
    }

    function setWhy($why) {
        $this->why = $why;
        return $this;
    }

    function setPhoto($photo) {
        $this->photo = $photo;
        return $this;
    }

}
