<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PostRepository")
 * @ORM\Table(name="cg_reward")
 */
class Reward {

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    private $skillId;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    private $pointsOfSkill;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    private $attributeId;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    private $pointsOfAttribute;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $checkedOfAttribute;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     * @Assert\DateTime
     */
    private $dateValidOfattribute;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set skillId
     *
     * @param integer $skillId
     *
     * @return Reward
     */
    public function setSkillId($skillId) {
        $this->skillId = $skillId;

        return $this;
    }

    /**
     * Get skillId
     *
     * @return integer
     */
    public function getSkillId() {
        return $this->skillId;
    }

    /**
     * Set pointsOfSkill
     *
     * @param integer $pointsOfSkill
     *
     * @return Reward
     */
    public function setPointsOfSkill($pointsOfSkill) {
        $this->pointsOfSkill = $pointsOfSkill;

        return $this;
    }

    /**
     * Get pointsOfSkill
     *
     * @return integer
     */
    public function getPointsOfSkill() {
        return $this->pointsOfSkill;
    }

    /**
     * Set attributeId
     *
     * @param integer $attributeId
     *
     * @return Reward
     */
    public function setAttributeId($attributeId) {
        $this->attributeId = $attributeId;

        return $this;
    }

    /**
     * Get attributeId
     *
     * @return integer
     */
    public function getAttributeId() {
        return $this->attributeId;
    }

    /**
     * Set pointsOfAttribute
     *
     * @param integer $pointsOfAttribute
     *
     * @return Reward
     */
    public function setPointsOfAttribute($pointsOfAttribute) {
        $this->pointsOfAttribute = $pointsOfAttribute;

        return $this;
    }

    /**
     * Get pointsOfAttribute
     *
     * @return integer
     */
    public function getPointsOfAttribute() {
        return $this->pointsOfAttribute;
    }

    /**
     * Set checkedOfAttribute
     *
     * @param boolean $checkedOfAttribute
     *
     * @return Reward
     */
    public function setCheckedOfAttribute($checkedOfAttribute) {
        $this->checkedOfAttribute = $checkedOfAttribute;

        return $this;
    }

    /**
     * Get checkedOfAttribute
     *
     * @return boolean
     */
    public function getCheckedOfAttribute() {
        return $this->checkedOfAttribute;
    }

    /**
     * Set dateValidOfattribute
     *
     * @param \DateTime $dateValidOfattribute
     *
     * @return Reward
     */
    public function setDateValidOfattribute($dateValidOfattribute) {
        $this->dateValidOfattribute = $dateValidOfattribute;

        return $this;
    }

    /**
     * Get dateValidOfattribute
     *
     * @return \DateTime
     */
    public function getDateValidOfattribute() {
        return $this->dateValidOfattribute;
    }

}
