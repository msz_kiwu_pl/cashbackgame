<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="cg_skill")
 */
class Skill {

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    private $categoryId;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    private $requiredAttributeId;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    private $requiredAttributeCategoryId;

    function getId() {
        return $this->id;
    }

    function getCategoryId() {
        return $this->categoryId;
    }

    function getRequiredAttributeId() {
        return $this->requiredAttributeId;
    }

    function getRequiredAttributeCategoryId() {
        return $this->requiredAttributeCategoryId;
    }

    function setId($id) {
        $this->id = $id;
        return $this;
    }

    function setCategoryId($categoryId) {
        $this->categoryId = $categoryId;
        return $this;
    }

    function setRequiredAttributeId($requiredAttributeId) {
        $this->requiredAttributeId = $requiredAttributeId;
        return $this;
    }

    function setRequiredAttributeCategoryId($requiredAttributeCategoryId) {
        $this->requiredAttributeCategoryId = $requiredAttributeCategoryId;
        return $this;
    }

}
