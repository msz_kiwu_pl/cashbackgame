<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="cg_skill_categories")
 */
class SkillCategories {

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", unique=true)
     */
    private $name;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    private $requiredAttributeId;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    private $requiredAttributeCategoryId;

    function getId() {
        return $this->id;
    }

    function getName() {
        return $this->name;
    }

    function getRequiredAttributeId() {
        return $this->requiredAttributeId;
    }

    function getRequiredAttributeCategoryId() {
        return $this->requiredAttributeCategoryId;
    }

    function setId($id) {
        $this->id = $id;
        return $this;
    }

    function setName($name) {
        $this->name = $name;
        return $this;
    }

    function setRequiredAttributeId($requiredAttributeId) {
        $this->requiredAttributeId = $requiredAttributeId;
        return $this;
    }

    function setRequiredAttributeCategoryId($requiredAttributeCategoryId) {
        $this->requiredAttributeCategoryId = $requiredAttributeCategoryId;
        return $this;
    }

}
