<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PostRepository")
 * @ORM\Table(name="cg_quest")
 */
class Quest {

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank
     */
    private $procedureOfDone;

    /**
     * @ORM\Column(type="string")
     *
     * @Assert\File(mimeTypes={ "image/jpeg" })
     */
    protected $photo;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    private $rewardId;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    private $requiredQuestId;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    private $requiredAttributeId;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    private $requiredAttributeCategoryId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     * @Assert\DateTime
     */
    private $validDate;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Quest
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Quest
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set procedureOfDone
     *
     * @param string $procedureOfDone
     *
     * @return Quest
     */
    public function setProcedureOfDone($procedureOfDone) {
        $this->procedureOfDone = $procedureOfDone;

        return $this;
    }

    /**
     * Get procedureOfDone
     *
     * @return string
     */
    public function getProcedureOfDone() {
        return $this->procedureOfDone;
    }

    /**
     * Set photo
     *
     * @param string $photo
     *
     * @return Quest
     */
    public function setPhoto($photo) {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return string
     */
    public function getPhoto() {
        return $this->photo;
    }

    /**
     * Set rewardId
     *
     * @param integer $rewardId
     *
     * @return Quest
     */
    public function setRewardId($rewardId) {
        $this->rewardId = $rewardId;

        return $this;
    }

    /**
     * Get rewardId
     *
     * @return integer
     */
    public function getRewardId() {
        return $this->rewardId;
    }

    /**
     * Set requiredQuestId
     *
     * @param integer $requiredQuestId
     *
     * @return Quest
     */
    public function setRequiredQuestId($requiredQuestId) {
        $this->requiredQuestId = $requiredQuestId;

        return $this;
    }

    /**
     * Get requiredQuestId
     *
     * @return integer
     */
    public function getRequiredQuestId() {
        return $this->requiredQuestId;
    }

    /**
     * Set requiredAttributeId
     *
     * @param integer $requiredAttributeId
     *
     * @return Quest
     */
    public function setRequiredAttributeId($requiredAttributeId) {
        $this->requiredAttributeId = $requiredAttributeId;

        return $this;
    }

    /**
     * Get requiredAttributeId
     *
     * @return integer
     */
    public function getRequiredAttributeId() {
        return $this->requiredAttributeId;
    }

    /**
     * Set requiredAttributeCategoryId
     *
     * @param integer $requiredAttributeCategoryId
     *
     * @return Quest
     */
    public function setRequiredAttributeCategoryId($requiredAttributeCategoryId) {
        $this->requiredAttributeCategoryId = $requiredAttributeCategoryId;

        return $this;
    }

    /**
     * Get requiredAttributeCategoryId
     *
     * @return integer
     */
    public function getRequiredAttributeCategoryId() {
        return $this->requiredAttributeCategoryId;
    }

    /**
     * Set validDate
     *
     * @param \DateTime $validDate
     *
     * @return Quest
     */
    public function setValidDate($validDate) {
        $this->validDate = $validDate;

        return $this;
    }

    /**
     * Get validDate
     *
     * @return \DateTime
     */
    public function getValidDate() {
        return $this->validDate;
    }

}
