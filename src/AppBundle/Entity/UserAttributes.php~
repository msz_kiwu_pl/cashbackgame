<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 * @ORM\Table(name="cg_user_attributes")
 */
class UserAttributes implements \JsonSerializable {

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    private $userId;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    private $attributeId;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $checked;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    private $points;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     * @Assert\DateTime
     */
    private $validDate;

    function getId() {
        return $this->id;
    }

    function getUserId() {
        return $this->userId;
    }

    function getAttributeId() {
        return $this->attributeId;
    }

    function getChecked() {
        return $this->checked;
    }

    function getPoints() {
        return $this->points;
    }

    function getValidDate() {
        return $this->validDate;
    }

    function setId($id) {
        $this->id = $id;
        return $this;
    }

    function setUserId($userId) {
        $this->userId = $userId;
        return $this;
    }

    function setAttributeId($attributeId) {
        $this->attributeId = $attributeId;
        return $this;
    }

    function setChecked($checked) {
        $this->checked = $checked;
        return $this;
    }

    function setPoints($points) {
        $this->points = $points;
        return $this;
    }

    function setValidDate(\DateTime $validDate) {
        $this->validDate = $validDate;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize() {
        // This entity implements JsonSerializable (http://php.net/manual/en/class.jsonserializable.php)
        // so this method is used to customize its JSON representation when json_encode()
        // is called, for example in tags|json_encode (app/Resources/views/form/fields.html.twig)

        return $this->name;
    }

    public function __toString() {
        return $this->name;
    }

}
